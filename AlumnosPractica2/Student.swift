//
//  Student.swift
//  AlumnosPractica2
//
//  Created by Ivan Cozar Laguna on 28/05/2019.
//  Copyright © 2019 Ivan Cozar Laguna. All rights reserved.
//

import Foundation
import GRDB

struct Student {
    // usar let porque es inmutable
    let name: String
    let surname: String
    let age: Int
    let approved: Bool
    let enrolment: String
}

extension Student: FetchableRecord, PersistableRecord{
    init(row: Row){
        name = row["name"]
        surname = row["surname"]
        age = row["age"]
        approved = row["approved"]
        enrolment = row["enrolment"]
    }
    
    func encode(to container: inout PersistenceContainer){
        container["enrolment"] = enrolment
        container["name"] = name
        container["surname"] = surname
        container["approved"] =   approved
        container["age"] = age
        
    }

}
