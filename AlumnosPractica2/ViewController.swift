//
//  ViewController.swift
//  AlumnosPractica2
//
//  Created by Ivan Cozar Laguna on 28/05/2019.
//  Copyright © 2019 Ivan Cozar Laguna. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var studentTable: UITableView!
    
    var students = [Student]()
    var studentSelected: Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentTable.register(UITableViewCell.self, forCellReuseIdentifier: "studentCellIdentifier")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        students = DataManager.dataManager.loadStudents()
        studentTable.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editStudentSegue" {
            if let studentViewController = segue.destination as? StudentViewController {
                studentViewController.student = studentSelected
                studentViewController.delegate = self
            }
            
        } else if segue.identifier == "addStudentSegue" {
            if let studentViewController = segue.destination as? StudentViewController {
                // limpiamos el estudiante
                studentSelected = nil
                
                studentViewController.delegate = self
            }
        }
    }
    

}

// El estandar de Apple es usar extensiones
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // El recibido
        let student = students[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCellIdentifier", for: indexPath)
        
        cell.textLabel?.text = student.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        studentSelected = students[indexPath.row]
        
        performSegue(withIdentifier: "editStudentSegue", sender: nil)
    }
    
    
}

extension ViewController: StudentViewControllerDelegate {
    
    func wasTappedOkButton() {
        students = DataManager.dataManager.loadStudents()
        studentTable.reloadData()
        
        dismiss(animated: true, completion: nil)
    }
    
    
}
