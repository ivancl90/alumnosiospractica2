//
//  DataManager.swift
//  AlumnosPractica2
//
//  Created by Ivan Cozar Laguna on 29/05/2019.
//  Copyright © 2019 Ivan Cozar Laguna. All rights reserved.
//

import Foundation
import GRDB

class DataManager {
    
    static let dataManager = DataManager()
    
    var dbQueue: DatabaseQueue?
    
    private init(){
        setupDataBase()
    }
    
    private func openDB() throws {
        let applicationSupportFolderUrl = try FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let dbUrl = applicationSupportFolderUrl.appendingPathComponent("demoDB.sqlite")
        
        dbQueue = try DatabaseQueue(path: dbUrl.path)
    }
    
    
    func setupDataBase() {
        
        do {
            try openDB()
            
            guard let dbQueue = dbQueue else {
                return
            }
            
            //SQLITE
            try dbQueue.write({ db in
                
                try db.create(table: "Student", temporary: false, ifNotExists: true, body: { t in
                    t.column("enrolment").primaryKey()
                    t.column("name")
                    t.column("surname")
                    t.column("age")
                    t.column("approved")
                })
                
            })
            
        }catch {
            print("Error al intentar crear la tabla Student \(error)")
        }
    }
    
    func saveStudent(_ student: Student){
        guard let dbQueue = dbQueue else {
            return
        }
        
        do {
            var enrolment: String?
            
            try dbQueue.read({ db in
                
                if let row = try Row.fetchOne(db, sql: "SELECT * FROM Student WHERE enrolment = ?", arguments: [student.enrolment]) {
                    enrolment = row["enrolment"]
                }
            })
            
            if enrolment != nil {
                
                try dbQueue.write { db in
                    try student.update(db)
                }
            } else  {
                
                try dbQueue.write { db in
                    try student.insert(db)
                }
            }
        } catch {
            print("Error al intentar guardar la tabla Student \(error)")
        }    }
    
    func loadStudents() -> [Student]{
        
        guard let dbQueue = dbQueue else {
            return [Student]()
        }
        
        do {
            return try dbQueue.read({ db -> [Student] in
                try Student.fetchAll(db, sql: "select * from Student")
            })
            
        } catch {
            print("Error al intentar recuperar la tabla Student \(error)")
        }
        
        return [Student]()
    }
    
}
